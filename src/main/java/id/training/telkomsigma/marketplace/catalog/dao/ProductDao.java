package id.training.telkomsigma.marketplace.catalog.dao;

import id.training.telkomsigma.marketplace.catalog.entity.Product;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductDao extends PagingAndSortingRepository<Product, String> {
}
